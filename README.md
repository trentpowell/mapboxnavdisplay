# NRC Mapbox Nav Display #

WIP mapbox app.

## Building MapLibreGL native ##

### Requirements ###
MSVS 2019+
[CMake](https://cmake.org/download/)
[Ninja](https://github.com/ninja-build/ninja/releases)
QT 5.15

** Maplibre is currently broken ** and won't display mapbox maps by default in Qt. The fix found [here](https://github.com/maplibre/maplibre-gl-native/pull/197) can either be implemented manually or using the pull request.

### MabLibre GL native Build instructions ###
	Open an admin version of vs native tools and enter the following:
	$ git clone --recursive https://github.com/maplibre/maplibre-gl-native.git
	$ cd maplibre-gl-native
	$ git fetch origin pull/197/head
	$ git checkout -b fix FETCH_HEAD
	$ cmake -G Ninja -DMBGL_WITH_QT=ON -DCMAKE_BUILD_TYPE=Release
	$ ninja
	$ ninja install

### Linking with QT ###

Make sure that the libraries are linked properly in qt. Change the following paths in the .pro file to be relevant to you if you changed them.

	LIBS += -L$$PWD/'../../../../Program Files (x86)/Mapbox GL Native/lib/' -lQMapboxGL
	INCLUDEPATH += $$PWD/'../../../../Program Files (x86)/Mapbox GL Native/include
	DEPENDPATH += $$PWD/'../../../../Program Files (x86)/Mapbox GL Native/include'

