#ifndef MAPMISSIONMANAGER_H
#define MAPMISSIONMANAGER_H

#include <QObject>
#include <QVariantMap>
class MapWindow;
class MapMissionManager : public QObject
{
    Q_OBJECT
public:
    explicit MapMissionManager(QObject *parent = nullptr,MapWindow* m = nullptr);
    void generateJson();
    bool active();
    QVariantMap* lineSource();
    QVariantMap* pointSource();

private:
    static double distanceOnMap(double,double,double,double);
    QVariantMap* m_lineSource;
    QVariantMap* m_pointSource;
    MapWindow* m_map;
    bool m_active;
public slots:
    void activate(bool);

};

#endif // MAPMISSIONMANAGER_H
