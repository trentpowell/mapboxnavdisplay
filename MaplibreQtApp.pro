QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    hudwindow.cpp \
    main.cpp \
    mainwindow.cpp \
    mapcontroller.cpp \
    mapmissionmanager.cpp \
    mapstylemanager.cpp \
    mapwindow.cpp \
    sharedmemoryinterpreter.cpp \
    viewtype.cpp

HEADERS += \
    hudwindow.h \
    mainwindow.h \
    mapcontroller.h \
    mapmissionmanager.h \
    mapstylemanager.h \
    mapwindow.hpp \
    mavNrc/config.h \
    mavNrc/frl_file_types.h \
    sharedmemoryinterpreter.h \
    viewtype.h

FORMS += \
    mainwindow.ui \
    maphud.ui \
    styleelement.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target



LIBS += -L'C:/Program Files (x86)/Mapbox GL Native/lib/' -lQMapboxGL

INCLUDEPATH += 'C:/Program Files (x86)/Mapbox GL Native/include'
DEPENDPATH += 'C:/Program Files (x86)/Mapbox GL Native/include'


unix|win32: LIBS += -L$$PWD/../drfrankencopter-mavnrc-63ce15f40438/drfrankencopter-mavnrc-63ce15f40438/x64/Release/ -lwinMavNrc

INCLUDEPATH += $$PWD/../drfrankencopter-mavnrc-63ce15f40438/drfrankencopter-mavnrc-63ce15f40438
DEPENDPATH += $$PWD/../drfrankencopter-mavnrc-63ce15f40438/drfrankencopter-mavnrc-63ce15f40438

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../drfrankencopter-mavnrc-63ce15f40438/drfrankencopter-mavnrc-63ce15f40438/x64/Release/winMavNrc.lib
else:unix|win32-g++: PRE_TARGETDEPS += $$PWD/../drfrankencopter-mavnrc-63ce15f40438/drfrankencopter-mavnrc-63ce15f40438/x64/Release/libwinMavNrc.a
