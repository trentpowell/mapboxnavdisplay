#include "mapmissionmanager.h"
#include "mapwindow.hpp"
#include "sharedmemoryinterpreter.h"
MapMissionManager::MapMissionManager(QObject *parent,MapWindow* map)
    : QObject{parent},m_map(map)
{
    m_active = false;
}

void MapMissionManager::generateJson()
{
    auto mission = m_map->sharedMemory()->mission();
    qDebug()<<mission;
    if(!mission){
        qDebug()<<"not connected";
        return;
    }
    auto items = mission->myCurrentMission.standard.items;
    auto items_len =mission->myCurrentMission.standard.numMissionItems;

    if(!items){
        qDebug()<<"no mission";
        return;
    }
    m_lineSource = new QVariantMap();
    m_pointSource = new QVariantMap();
    (*m_lineSource)["type"] = "FeatureCollection";
    (*m_pointSource)["type"] = "FeatureCollection";
    QVector<QMapbox::Feature> lineFeatures;
    QVector<QMapbox::Feature> pointFeatures;


    for(int i =0;i<items_len;i++){
        QPair<double,double> coordinate(items[i].x,items[i].y);
        QMapbox::Feature point{QMapbox::Feature::PointType,{{{coordinate}}},{{"z",items[i].z}}};
        pointFeatures.append(point);

    }
    (*m_pointSource)["features"] = QVariant::fromValue(*m_pointSource);

    for(int i =0;i<items_len-1;i++){
        QPair<double,double> coordinate(items[i].x,items[i].y);
        QPair<double,double> coordinate2(items[i+1].x,items[i+1].y);
        QMapbox::Feature line{QMapbox::Feature::LineStringType,{{{coordinate},{coordinate2}}},{{"length",distanceOnMap(items[i].x,items[i].x+1,items[i].y,items[i].y+1)}}};
        lineFeatures.append(line);
    }
    (*m_lineSource)["features"] = QVariant::fromValue(*m_lineSource);

    m_map->addSource("missionLineSource",*m_lineSource);
    m_map->addSource("missionPointSource",*m_lineSource);

}

bool MapMissionManager::active()
{
    return m_active;
}

QVariantMap *MapMissionManager::lineSource()
{
    return m_lineSource;
}

QVariantMap *MapMissionManager::pointSource()
{
    return m_pointSource;
}

double MapMissionManager::distanceOnMap(double lat1, double lat2,double lng1, double lng2){
    double r =6371;
    return 2*r* asin(sqrt(pow(sin((lat2-lat1)/2),2)+cos(lat1)*cos(lat2)*pow(sin((lng2-lng1)/2),2)));
}

void MapMissionManager::activate(bool value){
    m_active =value;
    if(value){
        generateJson();
    }
}
