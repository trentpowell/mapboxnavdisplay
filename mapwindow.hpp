#ifndef MAPWINDOW_H
#define MAPWINDOW_H

#include <QMapboxGL>

#include <QOpenGLWidget>
#include <QPropertyAnimation>
#include <QtGlobal>
#include <QJsonArray>
#include <memory>

class QKeyEvent;
class QMouseEvent;
class QWheelEvent;
class MapController;
class MapMissionManager;
class SharedMemoryInterpreter;

class MapWindow : public QOpenGLWidget
{
    Q_OBJECT

public:
    MapWindow(QWidget* parent = nullptr);
    ~MapWindow();
    void changeStyle();
    void moveMap(double,double);
    void zoomMap(double);
    QMapbox::Coordinate mapCords();
    void rotateMap(double degrees, QPointF origin);
    double mapRotation();
    void scaleMap(double scale, QPointF origin);
    void addSource(QString name, QVariantMap data);
    void addLayer(QVariantMap layer);
    void addLayer(QVariantMap layer, QString before);
    void removeLayer(QString id);
    bool hasLayer(QString);
    QJsonArray getLayers();
    SharedMemoryInterpreter* sharedMemory();
    void setMissionManager(MapMissionManager*);
    double lengthOfPixels(int pixels=1);
private:
    qreal pixelRatio();

    // QWidget implementation.
    void keyPressEvent(QKeyEvent *ev) final;
    void mousePressEvent(QMouseEvent *ev) final;
    void mouseMoveEvent(QMouseEvent *ev) final;
    void wheelEvent(QWheelEvent *ev) final;

    // Q{,Open}GLWidget implementation.
    void initializeGL() final;
    void paintGL() final;
private:
    QPointF m_lastPos;

    QMapboxGLSettings m_settings;
    std::unique_ptr<QMapboxGL> m_map{};
    std::unique_ptr<SharedMemoryInterpreter> m_memory;
    MapController* m_controller;
    MapMissionManager* m_missionManager;


    unsigned m_animationTicks = 0;
    unsigned m_frameDraws = 0;

    QVariant m_symbolAnnotationId;
    QVariant m_lineAnnotationId;
    QVariant m_fillAnnotationId;

    bool m_sourceAdded = false;
    void addMissionLayer();

signals:
    void onToggleController();
    void onZoomChanged(double zoom);
    void onRotationChanged(double rotation);
    void onPositionChanged(QMapbox::Coordinate position);
public slots:
    void loadLayers(QPair<QVector<bool>,QJsonArray>);

};

#endif
