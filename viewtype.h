#ifndef VIEWTYPE_H
#define VIEWTYPE_H

#include <QPair>


class MapWindow;
class ViewType
{
public:
    ViewType();
    virtual void updateMap(MapWindow* map,QPair<double,double> pos, double rot)=0;
};
class FollowHeading: public ViewType
{
public:
    FollowHeading(){};
    void updateMap(MapWindow* map,QPair<double,double> pos, double rot);
};

#endif // VIEWTYPE_H
