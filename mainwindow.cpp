#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "ui_styleelement.h"
#include "ui_maphud.h"
#include "sharedmemoryinterpreter.h"
#include "mapmissionmanager.h"
#include "mapwindow.hpp"
#include "mapstylemanager.h"
#include "qdebug.h"
#include <QScrollBar>
#include <QPushButton>
#include <QScrollArea>
#include <QStackedWidget>
#include <QFileDialog>
#include <QKeyEvent>
#include <QJsonObject>
#include <qpainter.h>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent), mainWindowUi(new Ui::MainWindow), m_map(new MapWindow(this))
{
    m_styleManager = new MapStyleManager(this,m_map);

    mainWindowUi->setupUi(this);
    setWindowTitle(QString("Mapbox NavDisplay"));
    m_map->setObjectName("map_Page");
    MapMissionManager *missionManager = new MapMissionManager(this,m_map);
    m_map->setMissionManager(missionManager);


    Ui::MapHud *hud = new Ui::MapHud();
    QWidget *hudWidget= new QWidget(m_map);
    hud->setupUi(hudWidget);
    hud->hud_Window->setHud(hud);
    connect(hud->mapBack_Button,&QPushButton::clicked,this,&MainWindow::showConfigView);
    connect(m_map,&MapWindow::onZoomChanged,hud->zoom_Text,[=](){
        hud->zoom_Text->setText("Length of 100 pixels:"+QString::number(m_map->lengthOfPixels(100))+"m");
    });

    //delete hud;
    mainWindowUi->stackedWidget->addWidget(m_map);
    connect(mainWindowUi->viewMap_Button,&QPushButton::clicked,m_map,[=](){m_map->loadLayers(m_styleManager->allLayers());});
    connect(mainWindowUi->viewMap_Button,&QPushButton::clicked,this,&MainWindow::showMapView);
    connect(mainWindowUi->tabWidget,&QTabWidget::currentChanged,this,&MainWindow::refresh);
    connect(mainWindowUi->styleTabRefresh_Button,&QPushButton::clicked,m_styleManager,&MapStyleManager::refreshLayerList);
    connect(mainWindowUi->styleTabRefresh_Button,&QPushButton::clicked,this,&MainWindow::refresh);
    connect(m_styleManager,&MapStyleManager::needStyleUpdate,this,&MainWindow::refresh);
    connect(mainWindowUi->addLayer_Button,&QPushButton::clicked,this,&MainWindow::openAddStyle);
    connect(mainWindowUi->addLayer_Button,&QPushButton::clicked,this,&MainWindow::refresh);
    connect(mainWindowUi->saveLayer_Button,&QPushButton::clicked,this,&MainWindow::openSaveStyle);
    connect(mainWindowUi->saveLayer_Button,&QPushButton::clicked,this,&MainWindow::refresh);
    connect(mainWindowUi->loadLayer_Button,&QPushButton::clicked,this,&MainWindow::openLoadStyle);
    connect(mainWindowUi->loadLayer_Button,&QPushButton::clicked,this,&MainWindow::refresh);

    connect(mainWindowUi->enableMission_CheckBox,&QCheckBox::toggled,missionManager,&MapMissionManager::activate);
    refresh();
}

MainWindow::~MainWindow()
{
    delete mainWindowUi;
    m_map->deleteLater();
    m_styleManager->deleteLater();
}

void MainWindow::keyPressEvent(QKeyEvent *ev)
{
    switch(ev->key()){
        case Qt::Key_Z:{
            m_map->loadLayers(m_styleManager->allLayers());
            break;
        }
        case Qt::Key_F:{
            if(isFullScreen())
            {
                //showMinimized();
                showNormal();
            }else{
                showFullScreen();
            }
            break;
        }
    }
}

void MainWindow::loadStyleView()
{
    int scrollval =mainWindowUi->styleTab_ScrollArea->verticalScrollBar()->value();//save scroll pos
    auto widgets =m_styleManager->styleWidgets();
    for(int i =0; i<widgets.count();i++){

        if(mainWindowUi->styleList_VL->children().contains(widgets[i])){//re-add if already exists
            mainWindowUi->styleList_VL->removeWidget(widgets[i]);
        }
        widgets[i]->setParent(mainWindowUi->styleTab_ScrollArea);
        mainWindowUi->styleList_VL->addWidget(widgets[i]);
    }
    mainWindowUi->styleTab_ScrollArea->verticalScrollBar()->setValue(scrollval);
}

void MainWindow::openAddStyle()
{
    QString filename = QFileDialog::getOpenFileName(this,"Open Json");
    m_styleManager->addCustomLayer(filename);
}

void MainWindow::openSaveStyle()
{
    QString filename = QFileDialog::getSaveFileName(this,"Save Config","/..","*.config");
    m_styleManager->saveStyle(filename);
}

void MainWindow::openLoadStyle()
{
    QString filename = QFileDialog::getOpenFileName(this,"Open Config");
    m_styleManager->loadStyle(filename);
}

void MainWindow::showMapView()
{
    mainWindowUi->stackedWidget->setCurrentWidget(m_map);
}

void MainWindow::showConfigView()
{
    mainWindowUi->stackedWidget->setCurrentWidget(mainWindowUi->config_Page);
}

void MainWindow::refresh()
{
    loadStyleView();
    m_styleManager->refreshLayerList();
}
