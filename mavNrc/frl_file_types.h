//
//  frl_file_types.h
//  iNS Test
//
//  Created by Kris Ellis on 11-09-26.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

// Probably will need to rename this as common.h as that seems to be more of what its function is
#ifndef FRL_FILE_TYPES_H
#define FRL_FILE_TYPES_H
#if defined(_WIN32) || defined(_WIN64)
	#ifndef WIN32_LEAN_AND_MEAN	// We need this to include winsocket.h and windows.h
	#define WIN32_LEAN_AND_MEAN	// see: https://docs.microsoft.com/en-us/windows/win32/winsock/creating-a-basic-winsock-application
								// This is best configured in the project settings as it needs to be declared before windows.h is included
	#endif
#else
	#include <sys/socket.h>	//Good ole BSD sockets
	#include <netinet/in.h>
#endif
#include <stdint.h>		 // Needed for fixed width types e.g. uint32_t
#include <stdio.h>       // added this to handle the use of the FILE specifier
#include "config.h"      // Ethernet ports, Sample rates and precision set here

// Basic setup variables
#define STACK_SIZE					65536
#define MAX_THREADS					20

//****************************************************************** Data Types Here **********************************
//KE got rid of legacy iOS stuff that was here.
// ****************************************************************** Network Data Types Below ************************
#pragma pack(push,4)
#ifdef DBL_TIME
typedef double gps_time_t;
#else
typedef float gps_time_t;
#endif

typedef struct {
#ifdef DBL_TIME
#ifdef SPLIT_DBL
    float time;
    float time_dec__;
#else
    double time;
#endif
#else
    float time;
#endif
	uint32_t status;
	float pitch;
	float roll;
    float hdg;//rotation
	float q;
	float p;
	float r;
	float ax;
	float ay;
	float az;
    float vn;//atan2(ve,vn) track (direction of velocity) don't use when velocity is very small (<1)
	float ve;
	float vz;
#ifdef SPLIT_DBL
    float lat_deg;
    float lat_dec;
    float lng_deg;
    float lng_dec;
#else
    double lat;
    double lng;//this one
#endif
	float alt;
#ifdef LASER_RAW
    float laser_raw;
#endif
#ifdef LASER_ALT
    float laser_alt;
    float mixedhgt;
#endif
#ifdef HDG_SIGMA
    float hdg_sigma;
#endif
#ifdef DVI_TIME
    float dvi_time;
#endif
#ifdef ALPHA_BETA
    float alpha;
    float beta;
#endif
} hg1700_t;
#pragma pack(pop)

// Harvard Analog Data Type Here
#pragma pack(push,4)
typedef struct {
	double	time;
	float	analog[28];
} harvard_analog_t;
#pragma pack(pop)

#pragma pack(push,4)
typedef struct {
	char signature[8];	// Just need 8 bytes
	harvard_analog_t analog_data;
} harvard_analog_rcv_t;		// Note that this struct has a signature type for receiving FRL type data
#pragma pack(pop)

#pragma pack(push,4)
typedef struct {
    double systime;
    uint32_t    vsw;    // virtual switches
    float   vp1_face;   // Pot face value 0-10
    float   vp2_face;
    float   vp3_face;
    float   vp4_face;
    float   vp5_face;
    float   vp6_face;
    float   vp7_face;
    float   vp8_face;
    float   vp9_face;
    float   vp10_face;
    float   vp11_face;
    float   vp12_face;
    float   vp1_value;  // Pot value in engineering units
    float   vp2_value;
    float   vp3_value;
    float   vp4_value;
    float   vp5_value;
    float   vp6_value;
    float   vp7_value;
    float   vp8_value;
    float   vp9_value;
    float   vp10_value;
    float   vp11_value;
    float   vp12_value;
    // Keep all strings at the end of the packet (for endian's sake)
    char    vp1_function[12];	// Current assigned function of pots
    char    vp2_function[12];   // Note current total #of chars is 144
    char    vp3_function[12];
    char    vp4_function[12];
    char    vp5_function[12];
    char    vp6_function[12];
    char    vp7_function[12];
    char    vp8_function[12];
    char    vp9_function[12];
    char    vp10_function[12];
    char    vp11_function[12];
    char    vp12_function[12];
    char	vsw0_function[12];
    char	vsw1_function[12];
    char	vsw2_function[12];
    char	vsw3_function[12];
    char	vsw4_function[12];
    char	vsw5_function[12];
    char	vsw6_function[12];
    char	vsw7_function[12];
} fcc_vpot_t;
#define FCC_VPOT_CHARS 240      // 12 pots and 12 chars each

#define VPOT_TYPE    0
#define VSWITCH_TYPE 1
typedef struct {
    uint32_t    control_type;   // 0=vpot, 1=vswitch
    uint32_t    control_number; // Pot number, or bit field number
    float       control_value;  // Pot value (0-10) or bit field value (0-1)
} vcontrol_adjust_t;    // Used to send a change in vpot value
#pragma pack(pop)

#pragma pack(push,4)
typedef struct { // contains the data collected by the CIF node
	double cif_system_time;
	uint32_t function_switches;
	uint32_t function_switches2;
	float sp_control_position_lateral;
	float sp_control_position_long;
	float sp_control_position_pedal;
	float sp_control_position_collective;
	float console_pot1;
	float console_pot2;
	float console_pot3;
	float console_pot4;
	float console_pot5;
	float console_pot6;
	float console_pot7;
	float console_pot8;
	float true_airspeed;
	float barometric_altimeter;
	float total_air_temp;
	float side_slip;
	float angle_of_attack;
	float collective_thumb_wheel_pot;
} cif_output_t; // called the same in Bell 205 ICD
#pragma pack(pop)

// Heres the struct definition for G500 data ... may need to copy this elsewhere
#pragma pack(push,4)
typedef struct {
    float selected_course;
    float selected_heading;
    float horizontal_command;
    float altitude;
    float altitude_baro_cor;
    float indicated_airspeed;
    float true_airspeed;
    float total_air_temp;
    float static_air_temp;
    float vertical_speed;
    float groundspeed;
    float track_angle;
    float true_heading;
    float mag_heading;
    float pitch_angle;
    float roll_angle;
    float pitch_rate;
    float roll_rate;
    float yaw_rate;
    float lat_accel;
    float normal_accel;
} g500_data_t;
#pragma pack(pop)

#pragma pack(push,4)
typedef struct {
    double  systime;
    float   pressure_alt;
    float     density_alt;
    float   true_airspeed;
    float   tat;
    float   oat;
    float   true_hdg;
    float   mag_hdg;
    float   theta;
    float   phi;
    float   q;
    float   p;
    float   r;
    float    lateral_deviation;
    float      glideslope_deviation;
    float     DME_distance;
    float     altitude_rate;
    float     VOR_omni_bearing;
    float     track_angle_true;
    float     wind_speed;
    float     wind_direction;
    float    ground_speed;
    float   cbox_oil_press;
    float   cbox_oil_temp;
    float     trans_oil_press;
    float   trans_oil_temp;
    float   eng1_t5_temp;
    float   eng2_t5_temp;
    float   eng1_fuel_press;
    float   eng2_fuel_press;
    float   eng1_ng;
    float   eng2_ng;
    float   eng1_oil_press;
    float   eng2_oil_press;
    float   eng1_oil_temp;
    float   eng2_oil_temp;
    float   eng1_np;
    float   eng2_np;
    float   eng1_q;
    float   eng2_q;
    float   generator1_dc_current;
    float   generator2_dc_current;
    float   generator1_dc_voltage;
    float   generator2_dc_voltage;
    float   inverter1_ac_voltage;
    float   inverter2_ac_voltage;
    float   sys1_hydraulic_press;
    float   sys2_hydraulic_press;
    float   sys1_hydraulic_temp;
    float   sys2_hydraulic_temp;
    float   mast_torque_right;
    float   mast_torque_left;
    float   rotor_speed;
    float   radalt_hi_res;
    float   radalt_low_res;
    float   left_fuel_quan;
    float   right_fuel_quan;
    float     total_fuel;
    float   total_fuel_flow;
    float   eng1_fuel_flow;
    float   eng2_fuel_flow;
    uint32_t     dau1_discretes1;
    uint32_t     dau1_discretes2;
    uint32_t     dau2_discretes1;
    uint32_t     dau2_discretes2;
    uint32_t     discretes_adc1;
    uint32_t     discretes_adc2;
    uint32_t     faultandmaintenance1;
    uint32_t     faultandmaintenance2;
} asra_arinc_data_t;
#pragma pack(pop)

#pragma pack(push,4)
typedef struct { //output packet from the fcc mainly for monitoring by the HMU
	double fcc_system_time;
	float final_drive_lateral;
	float final_drive_longitudinal;
	float final_drive_pedal;
	float final_drive_collective;
    uint32_t fcc_ok1;
    uint32_t fcc_ok2;
    float dlat_net;
    float dlon_net;
    float dped_net;
    float dcol_net;
} fcc_output_t; // called the same in Bell 205 ICD
#pragma pack(pop)


#pragma pack(push,4)
typedef struct { // provides configuration information mainly for display on the CDU
	double fcc_system_time;
	uint32_t fbw_engaged;
	uint32_t engagement_number;
	uint32_t control_index;
	float	cp1_value;	// pot values in engineering units as assigned by fcc_switches in fcc code
	float	cp2_value;
	float	cp3_value;
	float	cp4_value;
	float	cp5_value;
	float	cp6_value;
	float	cp7_value;
	float	cp8_value;
	char lateral_inceptor[12];
	char longitudinal_inceptor[12];
	char direction_inceptor[12];
	char heave_inceptor[12];
	char lateral_control_mode[16];
	char longitudinal_control_mode[16];
	char pedal_control_mode[16];
	char collective_control_mode[16];
	char cp1_function[12];	// Current assigned function of pots
	char cp2_function[12];
	char cp3_function[12];
	char cp4_function[12];
	char cp5_function[12];
	char cp6_function[12];
	char cp7_function[12];
	char cp8_function[12];
    char filename[12];
} fcc_configuration_t; // called the same in Bell 205 ICD
#pragma pack(pop)


#pragma pack(push,4)
typedef struct { // provides data only read by the HMU to other systems on the network
	double hmu_system_time;
	float fbw_actuator_position_lateral;
	float fbw_actuator_position_long;
	float fbw_actuator_position_pedal;
	float fbw_actuator_position_collective;
	float main_rotor_torque;
	float main_rotor_rpm;
	float radar_altimeter;
    //float dummy;    // test to see if aligning on 8 byte boundaries is better for data recording
} hmu_output_t; // called the same in Bell 205 ICD
#pragma pack(pop)


#pragma pack(push,4)
typedef struct { // Allows safety calculations on HMU to be recorded on FCC
	double hmu_system_time;
	uint32_t safety_status;
	float project_hydraulic_pressure;
	float project_hydraulic_temperature;
	float flap_angle; // In B412 this is tail plane incidence angle
	float lateral_command_rate;
	float longitudinal_command_rate;
	float pedal_command_rate;
	float collective_command_rate;
	float lateral_override;
	float longitudinal_override;
	float pedal_override;
	float collective_override;
    float cva_phi_pred;	// Predicted roll angle
    float cva_theta_pred;	// Predicted pitch angle
    float cva_alt_pred;
	float cva_tas;			// true airspeed used in cva calc
} hmu_safety_t; // called the same in Bell 205 ICD
#pragma pack(pop)

#pragma pack(push,4)
typedef struct { // contains all the data read by the FFS - mainly used by FCC as input to control systems
	double ffs_system_time;
	uint32_t ffs_switches;
	float ffs_control_position_lateral;
	float ffs_control_position_long;
	float ffs_control_position_pedal;
	float ffs_control_position_collective;
	float ffs_commanded_position_lateral;
	float ffs_commanded_position_long;
	float ffs_commanded_position_pedal;
	float ffs_commanded_position_collective;
	float ffs_applied_force_lateral;
	float ffs_applied_force_long;
	float ffs_applied_force_pedal;
	float ffs_applied_force_collective;
	float ffs_trim_position_lateral;
	float ffs_trim_position_long;
	float ffs_trim_position_pedal;
	float ffs_trim_position_collective;
	float sidearm_applied_force_lateral;
	float sidearm_applied_force_long;
	float sidearm_applied_force_pedal;
	float sidearm_applied_force_collective;
    float system2_hyd_press;
} ffs_output_t; // called the same in Bell 205 ICD
#pragma pack(pop)

#pragma pack(push,4)
typedef struct { // contains configuration info for the FFS to be displayed on the CDU
	double ffs_system_time;
	uint32_t ffs_engaged;
    float lateral_mass;
    float longitudinal_mass;
    float pedal_mass;
    float collective_mass;
	float lateral_gradient;
	float longitudinal_gradient;
	float pedal_gradient;
	float collective_gradient;
	float lateral_damping;
	float longitudinal_damping;
	float pedal_damping;
	float collective_damping;
	float lateral_breakout;
	float longitudinal_breakout;
	float pedal_breakout;
	float collective_breakout;
	float lateral_friction;
	float longitudinal_friction;
	float pedal_friction;
	float collective_friction;
    char ffs_stick_model[32];
} ffs_configuration_t; // called the same in Bell 205 ICD
#pragma pack(pop)

#pragma pack(push,4)
typedef struct { //provides raw gps data from the INS to assess gps performance
	double gps_time;
	double lattitude;
	double longitude;
	float altitude;
	float north_velocity;
	float east_velocity;
	float vertical_velocity;
	float north_sigma;
	float east_sigma;
	float vertical_sigma;
	uint32_t number_of_satelites;
	uint32_t position_type;
	float differential_age;
} ins_gps_performance_t; // called the same in Bell 205 ICD
#pragma pack(pop)

#pragma pack(push,4)
// packet broadcast to the data logger
typedef struct {    // Contains project specific data for recording on the data logger.
	double fcc_systime;
	uint32_t fcc_proj1_bits;  // used for control system mode identifications and other stuff...
	float   p_notch;   // Notch filtered rates
	float   q_notch;
	float   r_notch;
	float   ax_notch;  // Notch filtered accels
	float   ay_notch;
	float   az_notch;
	float   p_mix;     // Mixed rates
	float   q_mix;
	float   r_mix;
	float   u_gnd;     // Body axis velocities
	float   v_gnd;
	float   w_gnd;
	float   u_horz;    // Horizontal axis velocities
	float   v_horz;
	float   w_horz;
	float   u_gnd_dot; // Body axis rate of change of velocities
	float   v_gnd_dot;
	float   w_gnd_dot;
	float   u_horz_dot;// Horizontal axis rate of change of velocities
	float   v_horz_dot;
	float 	w_horz_dot;
	float	ground_speed;
	float	track_angle;
	float 	wind_speed;
	float 	wind_direction;
	float   phi_com;   // Commanded attitudes
	float   theta_com;
	float   psi_com;
	float   p_com;     // Commanded rates
	float   q_com;
	float   r_com;
	float   u_com;     // Commanded velocities
	float   v_com;
	float   w_com;     // For vert vel com
	double  latitude_com;
	double  longitude_com;
	float   h_com;     // Commanded height above ground
	float   proj1;	   // project specific variables	
	float   proj2;
	float   proj3;
	float   proj4;
	float   proj5;
	float   proj6;
	float   proj7;
	float   proj8;
	float   proj9;
	float   proj10;
	float   proj11;
	float   proj12;
	float	proj13;
	float	proj14;
	float	proj15;
	float	proj16;
	float	proj17;
	float	proj18;
	float	proj19;
	float	proj20;
} fcc_project_t; // Intended for data logger purposes only
#pragma pack(pop)  // end packing

// Audio Packet Tone
#pragma pack(push,4)
typedef struct {
    double hmu_system_time;     // This makes an assumption that all audio commands come from the hmu
    uint32_t   priority;
    char    tone_filename[32];  // this is the name of the audio file to play
} audio_command_packet_t;
#pragma pack(pop)

#pragma pack(push,4)
typedef struct {
	uint32_t flag;	// set to 1 to request shutdown
} shutdown_t;
#pragma pack(pop)

// Error Message Packet
#pragma pack(push,4)
typedef struct { // sends an error over the network for action by the hmu and recording by the fcc and display by the CDU
	double system_time;
	uint32_t error_identification_number;
	char error_message[32];
} error_send_t; // called the same in Bell 205 ICD
#pragma pack(pop)


// Packet type for sending recording status information
#pragma pack(push,4)
typedef struct {
	uint32_t	logger_id;		// 1= EP Mac mini 1, 2 = EP Mac Mini 2, 3 = Eng WS, others assigned later
	uint32_t	file_number;	// what filenumber is currently being recorded
	float	kb_logged;			// The number of kb logged to the file
} data_log_status_t;
#pragma pack(pop)

// air data from CIF
#pragma pack(push,4)
typedef struct {    // Contains raw air static and dynamic pressures
    float   ps;
    float   pd;
    float   ps_cor; // corrected after PEC
    float   pd_cor;
} air_data_t;
#pragma pack(pop)

#pragma pack(push,4)
typedef struct { // CVLAD steering commands from boeing laptop
    double system_time;
    uint32_t boeing_status;
    float long_accel_cmd;
    float lat_accel_cmd;
    float climb_rate_cmd;
    float turn_rate_cmd;
    float spare[8];
} cvlad_steering_t;
#pragma pack(pop)

#pragma pack(push,4)
typedef struct { // fcc status back to boeing laptop
    double system_time;
    uint32_t fcc_status;
    float spare[8];
} cvlad_fccstatus_t;
#pragma pack(pop)

#pragma pack(push,4)
typedef struct { // Simulator Configuration Packet
	uint32_t overrideSimPhysics; // Set to 1 if you want to override the simulator physics engine, 0 if you want to use the simulator physics engine
	uint32_t overrideSimEngines; // Set to 1 if you want to write into the engine parameters 
	uint32_t overrideSimActuators; // allows commands to be sent to the simulator's actuators and physics engine. will only work if overrideSimPhysics is 0
	uint32_t joystickButtons; // Stuffs the joystick buttons into a 32 bit field.
} simulatorConfig_t;
#pragma pack(pop)


// *********************************************************** BIG GLOBAL STRUCT HERE *********************************
#pragma pack(push,4)

typedef struct {
    hg1700_t hg1700;
    ins_gps_performance_t gps_data;
    ffs_output_t ffs_output;
    ffs_configuration_t ffs_config;
    fcc_output_t fcc_output;
    fcc_project_t fcc_project;
    fcc_configuration_t fcc_configuration;
    hmu_output_t hmu_output;
    hmu_safety_t hmu_safety;
    cif_output_t cif_output;
    air_data_t air_data;
#ifdef BELL_205
    g500_data_t g500_data;
#endif
#ifdef BELL_412
    asra_arinc_data_t asra_arinc_data;
#endif
    fcc_vpot_t fcc_vpot_data;
	simulatorConfig_t simulatorConfig;
} shared_data_t;
#pragma pack(pop)

#pragma pack(push,4)
typedef struct {
	hg1700_t ins_data;
	ins_gps_performance_t gps_data;
	ffs_output_t ffs_data;
	fcc_output_t fcc_data;
	fcc_project_t fcc_project;
	cif_output_t cif_data;
#ifdef BELL_205
	g500_data_t g500_data;
#endif
#ifdef BELL_412
	air_data_t	air_data;
	asra_arinc_data_t asra_arinc_data;
#endif
	hmu_output_t hmu_output_data;
	hmu_safety_t hmu_safety_data;
	simulatorConfig_t simulatorConfig;
} event_data_t;
#pragma pack(pop)

#pragma pack(push,4)
typedef struct {
    double      loggerSysTime;  // The local systime of the data logger This can be used to sync independantly logged files. Should be updated at 100Hz or so
    uint32_t    filenum; // just the number of the current data logger file
    char logfileDirectory[512]; // This should contain the directory name where logfiles are currently being written
                                // The intent is that the Data logger is the only program that writes to this shared
                                // memory block, and all other programs read from it. They should pause execution until a
                                // directory name is read from the shared mem block
} shared_logger_directory_t;
#pragma pack(pop)


#endif

