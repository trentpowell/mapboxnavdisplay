//
//  config.h
//  iNS Test
//
//  Created by Kris Ellis on 11-11-18.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

// Sample Rates


// set up the aircraft here
//#define EXTRA300
#define HELICOPTER
#define BELL_412	// For now. Probably best to make this a compile time define in the project

#ifdef EXTRA300
    #define DBL_TIME
    #define SPLIT_DBL
    #define HDG_SIGMA
    #define DVI_TIME
    #define HG1700_FSAMP    100
    #define REC_DIVISOR     1
#endif


#ifdef HELICOPTER
    // Precision
    #define DBL_TIME

    // Accessories
    #define LASER_ALT
    #define LASER_RAW

    
    #ifdef BELL_412
        #define HG1700_FSAMP	300
        #define REC_DIVISOR		1
        #define REGISTRATION    "PGV"
        #define HELO_NAME       "NRC Bell-412 C-FPGV"
    #elif defined BELL_205
        #define HG1700_FSAMP	133
        #define REC_DIVISOR		1
        #define REGISTRATION    "YZV"
        #define HELO_NAME       "NRC Bell-205 C-FYZV"
    #else
        #define HG1700_FSAMP	100
        #define REC_DIVISOR		1
        #define REGISTRATION    "ZUQ"
        #define HELO_NAME       "NRC Bell-206 C-FZUQ"
    #endif
#endif

// Ethernet Ports
#define HG1700_PORT     5124
#define HARVARD_ANALOG_PORT	5364
#define ERROR_SEND_PORT 6969
#define DATA_LOGGER_STATUS_PORT 5011
#define G500_ECHO_PORT 56767
#define FCC_PROJECT_PORT 5442 // Was 9999 in 205 code Nov 22 2019...but conflicts with ICD
#define FCC_VPOT_PORT   5127        // Port for fcc sendin current vpot assignments
#define FCC_VPOT_CHANGE_PORT 5126   // Port for sending a new vpot (or switch) value
#define IPAD_GYRO_PORT 7272
#define AIR_PORT 5158
#define CVLAD_STEERING_PORT                    4057
#define CVLAD_FCC_STATUS_PORT                4058

