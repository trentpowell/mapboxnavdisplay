#include "sharedmemoryinterpreter.h"


HANDLE hMapFile;
HANDLE hMapFileMission;
TCHAR sharedMemName[] = TEXT("asraSharedMem");
TCHAR missionDataSharedMemName[] = TEXT("missionDataMem");
#define BUF_SIZE sizeof(shared_data_t)

SharedMemoryInterpreter::SharedMemoryInterpreter()
{
    m_connected = connect();
    qDebug()<<"Connected ="<<m_connected;
}

bool SharedMemoryInterpreter::connected()
{
    return m_connected;
}

bool SharedMemoryInterpreter::connect()
{
    hMapFile = OpenFileMapping(
                FILE_MAP_READ,
                FALSE,
                sharedMemName);

    if (hMapFile == NULL)
    {
        //Could not create file mapping object
        return 0;
    }
    qDebug()<<"mapped main file..";
    m_sharedMemoryData = (shared_data_t *) MapViewOfFile(hMapFile,
                                                         FILE_MAP_READ, // read permission
                                                         0,
                                                         0,
                                                         BUF_SIZE);

    if (m_sharedMemoryData == NULL)
    {
        // Could not map view of file
        CloseHandle(hMapFile);
        return 0;
    }
    qDebug()<<"connected to main file..";
    //mission data
    /*
    hMapFileMission = OpenFileMapping(
                FILE_MAP_READ,
                FALSE,
                missionDataSharedMemName);

    if (hMapFileMission == NULL)
    {
        //Could not create file mapping object
        return 0;
    }
    qDebug()<<"mapped missison file..";
    m_missionData = (missionData_t*) MapViewOfFile(hMapFileMission,
                                                   FILE_MAP_READ, // read permission
                                                   0,
                                                   0,
                                                   BUF_SIZE);

    if (m_missionData == NULL)
    {
        // Could not map view of file
        CloseHandle(hMapFileMission);
        return 0;
    }
    qDebug()<<"connected to missison file..";
    */
    return 1;

}

QPair<double, double> SharedMemoryInterpreter::position()
{
    if(!m_sharedMemoryData)return QPair<double,double>{0,0};
    return QPair<double,double>{m_sharedMemoryData->hg1700.lat,m_sharedMemoryData->hg1700.lng};
}

shared_data_t *SharedMemoryInterpreter::data()
{
    return m_sharedMemoryData;
}



double SharedMemoryInterpreter::rotation()
{
    if(!m_connected)return 0;
    return m_sharedMemoryData->hg1700.hdg;
}


missionData_t *SharedMemoryInterpreter::mission()
{
    if(!m_connected)return nullptr;
    return m_missionData;
}
