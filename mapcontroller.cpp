#include "mapcontroller.h"
#include "mapwindow.hpp"
#include "sharedmemoryinterpreter.h"
#include "viewtype.h"
#include "QThread"
#include <qtimer.h>
#define MAP_REFRESH_RATE 5

MapController::MapController(QObject *parent, MapWindow* window)
    : QObject{parent},m_window(window)
{
    m_viewType.reset(new FollowHeading());
    m_thread.reset(new QThread);
    m_timer=new QTimer();
    connect(m_timer,&QTimer::timeout,this,&MapController::updateMap);
    connect(m_window,&MapWindow::onToggleController,this,&MapController::toggleLoop);
    this->moveToThread(m_thread.get());
    m_timer->moveToThread(m_thread.get());
    m_thread->start();
}

MapController::~MapController()
{

    if(m_thread->isRunning()){
        m_thread.release();
    }
    m_timer->deleteLater();
    m_viewType.release();
}

void MapController::toggleLoop()
{
    if(!m_timer->isActive()){
        qDebug()<<"starting timer";
        m_timer->start((1000/MAP_REFRESH_RATE));
    }
    else if(m_timer->isActive()){
        qDebug()<<"stoping timer";
        m_timer->stop();
    }
}

void MapController::setViewType(ViewType* type)
{
    m_viewType.reset(type);
}

bool MapController::isRunning()
{
    return m_timer->isActive();
}


void MapController::updateMap(){
    //qDebug()<<"Updating..";

    if(!m_window->sharedMemory()->connected())
        return;
    auto data = m_window->sharedMemory()->data();
    if(!data)
        return;
    m_viewType->updateMap(m_window, QPair<double,double>{data->hg1700.lat,data->hg1700.lng},data->hg1700.hdg);
    //zoom under conditions?
}


