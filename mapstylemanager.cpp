#include "mapstylemanager.h"
#include "mapwindow.hpp"
#include "ui_styleelement.h"
#include "mainwindow.h"
#include "QJsonObject"
#include "QJsonDocument"

#include <QFile>
#include <QMessageBox>
MapStyleManager::MapStyleManager(QWidget *parent,MapWindow* map)
    : QWidget{parent},m_customLayers(QVector<QString>()),m_map(map)
{

}

QPair<QVector<bool>, QJsonArray> MapStyleManager::allLayers()
{
return m_allLayers;
}


void MapStyleManager::refreshLayerList()
{
    QJsonArray potentiaLayers = m_map->getLayers();
    for(int i= 0; i<potentiaLayers.count();i++){
        if(!m_allLayers.second.contains(potentiaLayers[i])){
            m_allLayers.second.append(potentiaLayers[i]);
            m_allLayers.first.append(true);
        }
    }
    loadCustomLayers();
    for(int i =0; i<m_allLayers.second.count();i++){
        QString layerId =m_allLayers.second[i].toObject().value("id").toString();//id of layer at index
        bool existsFlag=false;
        for (int j=0;j<m_styleWidgets.count();j++ ) {//if object already created
            if(m_styleWidgets[j]->accessibleName()==layerId){
                existsFlag=true;
            }
        }
        if(existsFlag)continue;

        Ui::StyleElement* temp=new Ui::StyleElement();//create a new QWidget for list
        QWidget* item = new QWidget();
        temp->setupUi(item);//setup with template
        temp->label->setText(layerId);
        temp->checkBox->setCheckState(Qt::Checked);

        connect(temp->checkBox,&QCheckBox::stateChanged,this ,[=](int value){
            value ? addLayer(layerId):removeLayer(layerId);
        });//add and remove layer to map on toggle
        connect(temp->moveDown_Button,&QPushButton::clicked,this ,[=](){increasePriority(layerId);});//increase prio
        connect(temp->moveUp_Button,&QPushButton::clicked,this ,[=](){decreasePriority(layerId);});//decrease prio

        delete temp;
        item->setAccessibleName(layerId);
        m_styleWidgets.append(item);
    }

    syncWidgets();
}

QVector<QWidget *> MapStyleManager::styleWidgets()
{
    return m_styleWidgets;
}

bool MapStyleManager::isActive(QString id)
{
     for(int i =0; i<m_allLayers.second.count();i++){
         if(m_allLayers.second[i].toObject().value("id").toString()==id){
             return m_allLayers.first[i];
         }
     }
     return false;
}

void MapStyleManager::syncWidgets()
{
    for(int i =0; i<m_allLayers.second.count();i++){
        QString layerId =m_allLayers.second[i].toObject().value("id").toString();//id of layer at index
        int j;
        for(j =i; j<m_styleWidgets.count();j++){
            if(m_styleWidgets[j]->accessibleName()==layerId){
                break;
            }
        }
        auto temp = m_styleWidgets[j];
        m_styleWidgets[j] = m_styleWidgets[i];
        m_styleWidgets[i] = temp;
    }
}

void MapStyleManager::addCustomLayer(QString path)
{
    m_customLayers.append(path);
}

void MapStyleManager::addLayer(QString id)
{
    if(m_map->hasLayer(id))return;
    int i;
    for(i=0;i<m_allLayers.second.count();i++){
        if(m_allLayers.second[i].toObject().value("id").toString()==id) break;
    }

    if(m_allLayers.second.count()==i){
        qDebug()<<"Error adding layer";
        return;
    }

    m_allLayers.first[i]=true;

}

void MapStyleManager::removeLayer(QString id)
{
    if(!m_map->hasLayer(id))return;
    for(int i=0;i<m_allLayers.second.count();i++){
        if(m_allLayers.second[i].toObject().value("id").toString()==id) m_allLayers.first[i]=false;//bool list index false
    }
}

void MapStyleManager::increasePriority(QString id)
{
    int i;
    for(i=0;i<m_allLayers.second.count();i++){
        if(m_allLayers.second[i].toObject().value("id").toString()==id) break;
    }
    if(m_allLayers.second.count()==i+1){
        return;
    }

    //swap order in m_alllayers
    QJsonValue temp = m_allLayers.second[i+1];
    m_allLayers.second[i+1]=m_allLayers.second[i];
    m_allLayers.second[i]=temp;

    //swap order in widget
    auto temp2 = m_styleWidgets[i+1];
    m_styleWidgets[i+1]=m_styleWidgets[i];
    m_styleWidgets[i]=temp2;
    emit needStyleUpdate();

}

void MapStyleManager::decreasePriority(QString id)
{
    int i;
    for(i=0;i<m_allLayers.second.count();i++){
        if(m_allLayers.second[i].toObject().value("id").toString()==id) break;
    }
    if(i-1<0){
        return;
    }

    //swap order in m_alllayers
    QJsonValue temp = m_allLayers.second[i-1];
    m_allLayers.second[i-1]=m_allLayers.second[i];
    m_allLayers.second[i]=temp;

    //swap order in widget
    auto temp2 = m_styleWidgets[i-1];
    m_styleWidgets[i-1]=m_styleWidgets[i];
    m_styleWidgets[i]=temp2;
    emit needStyleUpdate();
}

void MapStyleManager::loadCustomLayers()
{
    for(int i=0;i<m_customLayers.count();i++){
        QFile file(m_customLayers[i]);
        if(!file.open(QIODevice::ReadOnly| QFile::Text)){
            QMessageBox::warning(this,"Error", "Cannot open file :"+file.errorString());
            return;
        }
        QTextStream in(&file);
        QJsonDocument doc =QJsonDocument::fromJson(in.readAll().toLatin1());
        if(doc.isNull()){
            QMessageBox::warning(this,"Error", "File "+file.fileName()+" is not valid JSON");
            return;
        }
        QJsonObject obj = doc.object();
        for(int i =0; i<m_allLayers.second.count();i++){
            if(m_allLayers.second[i].toObject().value("id").toString()==obj.value("id").toString()) continue;
        }
        if(!obj.contains("id")){
            obj["id"]=file.fileName();
        }
        qDebug()<<file.fileName();
        m_allLayers.second.append(obj);
        m_allLayers.first.append(true);
        file.close();
    }
}

void MapStyleManager::regenerateLayers()
{

}

void MapStyleManager::saveStyle(QString path)
{
    QFile file(path);
    if(!file.open(QFile::WriteOnly| QFile::Text)){
        QMessageBox::warning(this,"Error", "Cannot save file :"+file.errorString());
        return;
    }
    QTextStream out(&file);
    for(int i=0;i<m_allLayers.second.count();i++){
        out<< m_allLayers.first[i] <<","<< m_allLayers.second[i].toObject().value("id").toString()<<"\n";
    }
    file.close();

}

void MapStyleManager::loadStyle(QString path)
{
    QFile file(path);
    if(!file.open(QIODevice::ReadOnly| QFile::Text)){
        QMessageBox::warning(this,"Error", "Cannot open file :"+file.errorString());
        return;
    }
    QTextStream in(&file);
    QVector<QStringList> line;
    while(!in.atEnd()){
        line.append(in.readLine().split(","));
    }
    for(int i =0; i<line.count();i++){
        QString layerId =line[i][1];
        qDebug()<<layerId;
        qDebug()<<m_allLayers.second[i].toObject().value("id").toString();
        int j;
        for(j =i; j<m_allLayers.second.count();j++){
            if(m_allLayers.second[j].toObject().value("id").toString()==layerId){
                break;
            }
        }
        QJsonValue temp = m_allLayers.second[j];
        m_allLayers.second[j] = m_allLayers.second[i];
        m_allLayers.second[i] = temp;

        auto temp2 = m_allLayers.first[j];
        m_allLayers.first[j] = m_allLayers.first[i];
        m_allLayers.first[i] = temp2;
        qDebug()<<m_allLayers.second[i].toObject().value("id").toString();

    }

    syncWidgets();
    file.close();
}


