#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
namespace Ui {
class MainWindow;
}
class MapStyleManager;
class MapWindow;
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
private:
    Ui::MainWindow *mainWindowUi;
    MapWindow* m_map;
    MapStyleManager *m_styleManager;
private:
    void keyPressEvent(QKeyEvent *ev) final;

    void loadStyleView();
    void showMapView();
    void showConfigView();
private slots:
    void openAddStyle();
    void openSaveStyle();
    void openLoadStyle();
    void refresh();
};

#endif // MAINWINDOW_H
