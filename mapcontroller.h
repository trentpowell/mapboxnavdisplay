#ifndef MAPCONTROLLER_H
#define MAPCONTROLLER_H


#include <QObject>
#include <memory>
#include "viewtype.h"
class MapWindow;
class MapController : public QObject
{
    Q_OBJECT
public:
    explicit MapController(QObject *parent = nullptr,MapWindow* window =nullptr);
    ~MapController();
    void setViewType(ViewType*);
    bool isRunning();
private:
    MapWindow* m_window;
    std::unique_ptr<QThread> m_thread;
    std::unique_ptr<ViewType> m_viewType;
    QTimer* m_timer;

private slots:
    void updateMap();
public slots:
    void toggleLoop();

};

#endif // MAPCONTROLLER_H
