#ifndef MAPSTYLEMANAGER_H
#define MAPSTYLEMANAGER_H

#include <QWidget>
#include <QJsonArray>
namespace Ui{
class StyleElement;
}
class MapWindow;
class MapStyleManager : public QWidget
{
    Q_OBJECT
public:
    struct mapLayer{//currently unused
        QVariantMap value;
        bool active;

    };
private:
    QPair<QVector<bool>,QJsonArray> m_allLayers;//bool vector keeps track of currently active layers.
    QVector<QString> m_customLayers;//vector of paths to layer json
    QVector<QWidget*> m_styleWidgets;
    MapWindow* m_map;
public:
    explicit MapStyleManager(QWidget *parent = nullptr, MapWindow* map = nullptr);
    QPair<QVector<bool>,QJsonArray> allLayers();
    QVector<QWidget*> styleWidgets();
    bool isActive(QString id);
private:
    void syncWidgets();
    void addLayer(QString id);
    void removeLayer(QString id);
    void increasePriority(QString id);
    void decreasePriority(QString id);
    void loadCustomLayers();
public slots:
    void regenerateLayers();
    void refreshLayerList();
    void addCustomLayer(QString path);
    void saveStyle(QString path);
    void loadStyle(QString path);
signals:
    void needStyleUpdate();
};

#endif // MAPSTYLEMANAGER_H
