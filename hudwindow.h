#ifndef HUDWINDOW_H
#define HUDWINDOW_H

#include <QWidget>
namespace Ui {
class MapHud;
}
class HudWindow : public QWidget
{
    Q_OBJECT
public:
    explicit HudWindow(QWidget *parent = nullptr);
    void setHud(Ui::MapHud*);
private:
    Ui::MapHud* m_hud;
protected:
    void paintEvent(QPaintEvent *ev);
signals:

};

#endif // HUDWINDOW_H
