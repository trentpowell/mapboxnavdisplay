#include "layerdisplayview.h"

LayerDisplayView::LayerDisplayView()
{

}


QRect LayerDisplayView::visualRect(const QModelIndex &index) const
{
}

void LayerDisplayView::scrollTo(const QModelIndex &index, ScrollHint hint)
{
}

QModelIndex LayerDisplayView::indexAt(const QPoint &point) const
{
}

QModelIndex LayerDisplayView::moveCursor(CursorAction cursorAction, Qt::KeyboardModifiers modifiers)
{
}

int LayerDisplayView::horizontalOffset() const
{
}

int LayerDisplayView::verticalOffset() const
{
}

bool LayerDisplayView::isIndexHidden(const QModelIndex &index) const
{
}

void LayerDisplayView::setSelection(const QRect &rect, QItemSelectionModel::SelectionFlags command)
{
}

QRegion LayerDisplayView::visualRegionForSelection(const QItemSelection &selection) const
{
}
