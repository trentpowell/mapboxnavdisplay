#ifndef LAYERDISPLAYVIEW_H
#define LAYERDISPLAYVIEW_H
#include <QAbstractItemView>

class LayerDisplayView :QAbstractItemView
{
public:
    LayerDisplayView();

    // QAbstractItemView interface
public:
    QRect visualRect(const QModelIndex &index) const;
    void scrollTo(const QModelIndex &index, ScrollHint hint);
    QModelIndex indexAt(const QPoint &point) const;

protected:
    QModelIndex moveCursor(CursorAction cursorAction, Qt::KeyboardModifiers modifiers);
    int horizontalOffset() const;
    int verticalOffset() const;
    bool isIndexHidden(const QModelIndex &index) const;
    void setSelection(const QRect &rect, QItemSelectionModel::SelectionFlags command);
    QRegion visualRegionForSelection(const QItemSelection &selection) const;
};

#endif // LAYERDISPLAYVIEW_H
