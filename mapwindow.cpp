#include "mapwindow.hpp"
#include "mapcontroller.h"
#include "sharedmemoryinterpreter.h"
#include "mapmissionmanager.h"
#include <QApplication>
#include <QColor>
#include <QDebug>
#include <QPainter>
#include <QFile>
#include <QIcon>
#include <QKeyEvent>
#include <QMouseEvent>
#include <QString>
#include <qthread.h>
#include <QJsonDocument>
#include <QJsonObject>



MapWindow::MapWindow(QWidget* parent): QOpenGLWidget(parent)
{
    setWindowIcon(QIcon(":icon.png"));
    m_controller = new MapController(nullptr,this);
    m_memory.reset(new SharedMemoryInterpreter());

    //TODO: some sort of cached settings
    m_settings.setCacheDatabasePath("C:/Users/powellt/Desktop/test/mbgl-cache.db");
    m_settings.setCacheDatabaseMaximumSize(20 * 1024 * 1024);
    m_settings.resetToTemplate(QMapboxGLSettings::MapboxSettings);
    m_settings.setApiKey("pk.eyJ1IjoiY3ZsYWQiLCJhIjoiY2t5YnU5ZWFpMDBteTJvcGF5dGJyMXczeCJ9.yuupYYFrEW7-mNY7j213Wg");

    m_map.reset(new QMapboxGL(nullptr, m_settings, size(), pixelRatio()));

    m_map->setCoordinateZoom(QMapbox::Coordinate(45.33, -75.65), 6);
    QString styleUrl = "mapbox://styles/cvlad/ckyke0j502hf915kwph2xwkp7";
    m_map->setStyleUrl(styleUrl);
}

MapWindow::~MapWindow()
{
    // Make sure we have a valid context so we
    // can delete the QMapboxGL.
    makeCurrent();
    delete m_controller;
}

void MapWindow::changeStyle()
{
    static uint8_t currentStyleIndex;
    auto& styles = m_map->defaultStyles();
    m_map->setStyleUrl(styles[currentStyleIndex].first);

    if (++currentStyleIndex == styles.size()) {
        currentStyleIndex = 0;
    }

    m_sourceAdded = false;
}
void MapWindow::moveMap(double x, double y)
{
    m_map->setCoordinate({x,y});
}

void MapWindow::zoomMap(double zoom)
{
    m_map->setZoom(zoom);
    emit onZoomChanged(m_map->zoom());
}

void MapWindow::rotateMap(double degrees, QPointF origin)
{
    m_map->setBearing(degrees,origin);
}

void MapWindow::scaleMap(double scale, QPointF origin)
{
    m_map->scaleBy(scale,origin);
}

void MapWindow::addSource(QString name, QVariantMap data)
{
    if(m_map->sourceExists(name)){
        m_map->removeSource(name);
    }
    m_map->addSource(name,data);
}

void MapWindow::addLayer(QVariantMap layer)
{
     m_map->addLayer(layer);
}

QMapbox::Coordinate MapWindow::mapCords()
{
    return m_map->coordinate();
}

bool MapWindow::hasLayer(QString id)
{
    return m_map->layerExists(id);
}

QJsonArray MapWindow::getLayers()
{
    return QJsonDocument::fromJson(m_map->styleJson().toLocal8Bit()).object().value("layers").toArray();
}

SharedMemoryInterpreter* MapWindow::sharedMemory()
{
    return m_memory.get();
}

void MapWindow::setMissionManager(MapMissionManager * missionManager)
{
    m_missionManager = missionManager;
}

double MapWindow::lengthOfPixels(int pixels)
{
    return m_map->metersPerPixelAtLatitude(m_map->latitude(),m_map->zoom()) * pixels;
}

double MapWindow::mapRotation()
{
    return m_map->bearing();
}

qreal MapWindow::pixelRatio() {
    return devicePixelRatioF();
}

void MapWindow::keyPressEvent(QKeyEvent *ev)//default implemenation; todo
{
    switch (ev->key()) {
    case Qt::Key_0:{
        qDebug()<<"Controller Toggled";
        emit onToggleController();
        break;
    }
    default:{
        break;
    }
    }


    ev->accept();
}

void MapWindow::mousePressEvent(QMouseEvent *ev)//default implemenation; todo
{

    m_lastPos = ev->localPos();
    if (ev->type() == QEvent::MouseButtonDblClick) {
        if (ev->buttons() == Qt::LeftButton) {
            m_map->scaleBy(2.0, m_lastPos);
        } else if (ev->buttons() == Qt::RightButton) {
            m_map->scaleBy(0.5, m_lastPos);
        }
    }

    ev->accept();
}

void MapWindow::mouseMoveEvent(QMouseEvent *ev)//default implemenation; todo
{

    const QPointF &position = ev->localPos();
    QPointF delta = position - m_lastPos;
    if (!delta.isNull()) {
        if (ev->buttons() == Qt::LeftButton && ev->modifiers() & Qt::ShiftModifier) {
            m_map->pitchBy(delta.y());
        } else if (ev->buttons() == Qt::LeftButton) {
            m_map->moveBy(delta);
        } else if (ev->buttons() == Qt::RightButton) {
            m_map->rotateBy(m_lastPos, position);
        }
    }

    m_lastPos = position;
    ev->accept();
}

void MapWindow::wheelEvent(QWheelEvent *ev)//default implemenation; todo
{
    if (ev->angleDelta().y() == 0) {
        return;
    }

    float factor = ev->angleDelta().y() / 1200.;
    if (ev->angleDelta().y() < 0) {
        factor = factor > -1 ? factor : 1 / factor;
    }
    if(m_controller->isRunning()){
        m_map->scaleBy(1 + factor, ev->position());
    }else{
        m_map->scaleBy(1 + factor, ev->position());
    }
    ev->accept();

    emit onZoomChanged(m_map->zoom());
}

void MapWindow::initializeGL()
{

    connect(m_map.get(), SIGNAL(needsRendering()), this, SLOT(update()));
    qDebug()<<"map init";
}

void MapWindow::paintGL()
{
    m_frameDraws++;
    m_map->resize(size());
    m_map->setFramebufferObject(defaultFramebufferObject(), size() * pixelRatio());
    m_map->render();

}


void MapWindow::addMissionLayer()
{
    if(m_missionManager->active()){
        m_map->addLayer({
            {"id", "missionLineLayer"},
            {"type", "line"},
            {"source", "missionLineSource"}
        });
        m_map->addLayer({
            {"id", "missionPointLayer"},
            {"type", "circle"},
            {"source", "missionPointSource"}
        });
        m_map->setPaintProperty("missionLineLayer", "line-color", QColor("purple"));
        m_map->setPaintProperty("missionPointLayer", "circle-color", QColor("purple"));

    }
}

void MapWindow::loadLayers(QPair<QVector<bool>, QJsonArray> layers)
{
    QJsonObject json = QJsonDocument::fromJson(m_map->styleJson().toLocal8Bit()).object();
    json.remove("layers");
    QJsonArray arr = QJsonArray();
    for(int i=0;i<layers.second.count();i++){
        if(layers.first[i]){
            arr.append(layers.second[i]);
        }
    }
    qDebug()<<"Why";
    json["layers"] = arr;
    addMissionLayer();
    m_map->setStyleJson(QJsonDocument(json).toJson());
}
