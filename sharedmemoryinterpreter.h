#pragma once
#ifndef SHAREDMEMORYINTERPRETER_H
#define SHAREDMEMORYINTERPRETER_H
#include <QPair>
#include "QtDebug"
#include "winMavNrc/globals.h"
#include "mavNrc/frl_file_types.h"

class SharedMemoryInterpreter
{
public:
    SharedMemoryInterpreter();
    ~SharedMemoryInterpreter(){};
    bool connected();
    double rotation();
    QPair<double,double> position();
    shared_data_t* data();
    missionData_t* mission();
private:
    bool connect();
    bool m_connected = false;
    shared_data_t* m_sharedMemoryData;
    missionData_t* m_missionData;
};
#endif // SHAREDMEMORYINTERPRETER_H
